less中文网站
--http://lesscss.net
--less中文旧版网站
  -http://old.lesscss.net/article/document.html

less 类似于jquery
LESS CSS是一种动态样式语言，属于CSS预处理语言的一种，它使用类似CSS的语法，为CSS的赋予了动态语言的特性，如变量、继承、运算、函数登，更方别CSS的编写和维护

编译工具
1-koala编译
--koala
--国人开发的LESS/SASS编译工具
--下载地址 http://koala-app.com/index-zh.html
2-Node.js库
3-浏览器端的使用

LESS中的注释
/*我是被编译的*/
//不会被编译

变量
@test_width: 300px;
.box{
  width: @test_width;
  height: @test_width;
  background-color:yellow;
}


####################
混合
.box{
  .border;
}
.border{
  border: 5px solid pink;
}
====>编译后
.box{
  border: 5px solid pink;
}
//混合可带参数的
.border_02(@border_width){
  border: @border_width solid yellow;
}
.test_hunhe{
  .border_02(30px);
}
//混合-默认带值
.border_03(@border_width:10px){
  border: @border_width solid yellow;
}
.test_hunhe_03{
  .border_03(20px);
}
//混合例子
.border_radius(@radius:5px){

  border-radius:@radius;
  -webkit-border-radius:@radius;
  -moz-border-radius:@radius;
}
.radius_test{
  width:100px;
  height:40px;
  background-color: green;
  .border_radius();
}
####################


####################
匹配模式
.triangle(top,@w:5px,@c:#ccc){
  border-width: @w;
  border-color: transparent transparent @c transparent;
  border-style: dashed dashed solid dashed;
}
.triangle(bottom,@w:5px,@c:#ccc){
  border-width: @w;
  border-color: @c transparent transparent  transparent;
  border-style: dashed dashed solid dashed;
}
.triangle(left,@w:5px,@c:#ccc){
  border-width: @w;
  border-color: transparent @c transparent  transparent;
  border-style: dashed dashed solid dashed;
}
.triangle(right,@w:5px,@c:#ccc){
  border-width: @w;
  border-color: transparent transparent  transparent @c;
  border-style: dashed dashed solid dashed;
}
.triangle(@_,@w:5px,@c:#ccc){
  width: 0;
  height: 0;
  overflow: hidden;
}

.sanjiao{
  .triangle(top,100px);
}
//匹配模式的定位
//匹配模式-定位
.pos(r){
  position: relative;
}
.pos(a){
  position: absolute;
}
.pos(f){
  position: fix;
}
.pipei{
  width: 200px;
  height: 200px;
  background-color: green;
  .pos(f);
}
####################


####################
运算
less中的运算
-任何数字、颜色或者变量都可以参与运算，运算应该被包裹在括号中
例如： + - * /

@test_01:100px;

.box_02{
  height: 100px;
  width:(@test_01 - 20)*5;
  background: #ff0;
}
注意减号空格

####################


####################


####################



####################
@arguments变量
@arguments包含了所有传递进来的参数
如果你不想单独处理每一个参数的话就可以像这样写

.border_arg(@w:30px,@c:red,@xx:solid){
  border: @arguments;
}

.test_arguments{
  .border_arg();
}
####################

####################
避免编译
-有时候我们需要输出一些不正确的CSS语法或者使用一些LESS不认识的专有语法
-要输出这样的值我们可以在字符串钱加上一个 ~
例如： width: ~' calc(100%- 35)'
//避免编译
.test_03{
  width: ~'calc(300px - 30px)';
}

!important关键字
//important
.test_important{
  .border_radius() !important;
}


####################

















####################
